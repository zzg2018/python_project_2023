# django-test

## build

```bash
python3 -m build .
```
when build finishes, you can find the package under `./dist` folder.

## installation

```bash
pip3 install ./dist/django_atest-0.0.1.tar.gz

pip3 install ./dist/django_atest-0.0.1-py3-none-any.whl
```
Uninstallation:
```bash
pip3 uninstall django_atest
```

## enable and adjust behaviours

In order to enable the middleware, you will need to add it to the `MIDDLEWARE` list.

```python
MIDDLEWARE = [
    'django_atest.middleware.CountRequestsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    ...
]
```

## Test
```bash
cd tests
cd Django-Middleware-App
python manage.py migrate
python manage.py makemigrations
python manage.py runserver
```
http://127.0.0.1:8000/


