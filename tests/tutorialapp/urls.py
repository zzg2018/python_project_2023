from django.urls import path

from tutorialapp.views import hello

urlpatterns = [
    path("hello/", hello),
]