from django.test import TestCase


# Create your tests here.
class HelloTestCase(TestCase):

    def test_hello(self):
        response = self.client.get('/hello/')
        self.assertEqual(response.status_code, 200)
