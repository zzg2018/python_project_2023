from django.http import HttpResponse


def hello(request, *args, **kwargs):
    return HttpResponse(f"Hello!", status=200)
